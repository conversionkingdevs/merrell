var information = {
                        "tiles": {
                            "Womens": {
                                "image": "//cdn.optimizely.com/img/6092490016/ea8e4e14728941e99b421ef2d5f0517f.png",
                                "mobileImage": "//cdn.optimizely.com/img/6092490016/7920f75b88ec4a5ba376d8aec9446ca7.png",
                                "link": "http://www.merrellaustralia.com.au/women/view-all",
                                "title": "Women's"
                            },
                            "Mens": {
                                "image": "//cdn.optimizely.com/img/6092490016/9a70280ee93046939ce5a3ccc22f755c.png",
                                "mobileImage": "//cdn.optimizely.com/img/6092490016/ba9e398010464481b1059265c80d8314.png",
                                "link": "http://www.merrellaustralia.com.au/shop/mens",
                                "title": "Men's"
                            },
                            "newin": {
                                "image": "//cdn.optimizely.com/img/6092490016/ee3d0be9191245bca257ad5340a251ba.png",
                                "mobileImage": "//cdn.optimizely.com/img/6092490016/06ad34a5e8ae4fbdb207d965fa96e487.png",
                                "link": "http://www.merrellaustralia.com.au/shop/new-arrivals",
                                "title": "New In",
                                "message": "Shop our new styles"
                            },
                            "hiking": {
                                "image": "//cdn.optimizely.com/img/6092490016/b31bc2600fa94f81b07c89faaa65c03e.png",
                                "mobileImage": "//cdn.optimizely.com/img/6092490016/b9ad57c02dd24224a3b198fb31414539.png",
                                "link": "http://www.merrellaustralia.com.au/shop/hike",
                                "title": "Hiking",
                                "message": "Outdoor performance on duty"
                            },
                            "sale": {
                                "image": "//cdn.optimizely.com/img/6092490016/38caa702011447399c7a103dcc5f2b7d.png",
                                "mobileImage": "//cdn.optimizely.com/img/6092490016/5ee15b9bacc6470fa38ab9b54fbb204c.png",
                                "link": "http://www.merrellaustralia.com.au/shop/sale",
                                "title": "Sale",
                                "message": "Quality products at a discount"
                            }
                        }
                    },
    bannerHTML = '<div class="optMainBannerCont"> <div class="optLeft" style="background-image: url('+information.tiles.Womens.image+')"> <div class="tileContainer"> <div class="optWhite"> <span>'+information.tiles.Womens.title+'</span> </div><a class="optOrangeButton" href="'+information.tiles.Womens.link+'"> SHOP NOW </a> </div></div><div class="optRight" style="background-image: url('+information.tiles.Mens.image+')"> <div class="tileContainer"> <div class="optWhite"> <span>'+information.tiles.Mens.title+'</span> </div><a class="optOrangeButton" href="'+information.tiles.Mens.link+'"> SHOP NOW </a> </div></div></div>',
    tileHTML,
    mobileHTML;

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function createTiles (data) {
    var html = '';
    for (var x in data) {
        console.log(x);
        if ( x != "Mens" && x != "Womens" ) {
            console.log(x);
        var image = data[x].image,
            link = data[x].link,
            title = data[x].title,
            desc = data[x].message,
            tile = '<div class="optTile"> <div class="optImage"> <img src="'+image+'"> </div><div class="optTitleContent"> <p class="optTitle">'+title+'</p><p class="optDesc">'+desc+'</p></div><a class="optOrangeButton" href="'+link+'"> SHOP NOW </a> </div>';
            html += tile;
        }
    }
    return(html);
}

function createMobileTiles (data) {
    var html = '';
    for (var x in data) {
        var image = data[x].mobileImage,
            link = data[x].link,
            title = data[x].title;
            tile = '<div class="optTileM"> <div class="optTileMCont"> <img src="'+image+'"> <div class="optTitleContent"> <p class="optTitle">'+title+'</p><a class="optOrangeButton" href="'+link+'"> SHOP NOW </a> </div></div></div>';
            html += tile;
    }
    return(html);
}

function changeHTML (div, newhtml) {
    jQuery(div).html(newhtml);
}

function addHTML(div, newhtml, type) {
    switch(type) {
        case "after":
            jQuery(div).after(newhtml);
            break;
        case "before":
            jQuery(div).before(newhtml);
            break;
        case "append":
            jQuery(div).append(newhtml);
            break;
        case "prepend":
            jQuery(div).prepend(newhtml);
            break;
        default:
            console.log('Define a type');
    }
}

function addClass (div, addClass) {
    jQuery(div).addClass(addClass);
}


function start () {
  var tileHTML = '<div class="optTileContainer">'+createTiles(information.tiles)+'</div>';
  var mobileHTML = '<div class="optMobileTiles optMobile">'+createMobileTiles(information.tiles)+'</div>';

  changeHTML('.afeature-slider-container', bannerHTML);
  changeHTML('.homepage-block', tileHTML);
  addHTML('.col-main', mobileHTML, "prepend");

  addClass('.afeature-slider-container, .nav-container, .optTileContainer, .homepage-block', 'optDesktop');
}

defer(start, '.afeature-slider-container');

