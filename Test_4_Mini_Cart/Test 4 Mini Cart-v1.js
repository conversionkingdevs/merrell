
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer (function(){
    jQuery('body').addClass('opt4-v1');
    /**************************************
        LAYOUT V1
    **************************************/ 
    if(jQuery(".opt4-v1 #topCartContent").length > 0 && jQuery(".opt4-v1 #topCartContent .custom-checkout-link").length < 1){
        jQuery(".opt4-v1 #topCartContent .actions").prepend('<a class="custom-checkout-link" href="https://www.merrellaustralia.com.au/checkout/onepage/">Checkout</a>');
    };

    jQuery(document).ajaxComplete(function() {
        if(jQuery(".opt4-v1 #topCartContent").length > 0 && jQuery(".opt4-v1 #topCartContent .custom-checkout-link").length < 1){
            jQuery(".opt4-v1 #topCartContent .actions").prepend('<a class="custom-checkout-link" href="https://www.merrellaustralia.com.au/checkout/onepage/">Checkout</a>');
        };
    });
}, "#topCartContent .inner-wrapper");