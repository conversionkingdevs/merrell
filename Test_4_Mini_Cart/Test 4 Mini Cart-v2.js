function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer (function(){
    jQuery('body').addClass('opt4-v2');

    /**************************************
        LAYOUT V2
    **************************************/ 
    var $ajaxEnabled = false;
    jQuery(".opt4-v2").append('<div id="sidebar-cart-overlay" />');
    jQuery(".opt4-v2").append('<aside id="sidebar-cart"><div class="top-cart"></div><div class="sidebar-cart-inner"></div></aside>')
    $sidebar = jQuery("#sidebar-cart .sidebar-cart-inner");
    $sidebar.append('<a class="sidebar-cart-close-button" href="#"></h2>');
    $sidebar.append('<h2 class="sidebar-cart-title">Recently added item(s)</h2>');
    $sidebar.append('<div id="sidebar-cart-items" />');
    $sidebar.find('#sidebar-cart-items').html(jQuery("#topCartContent .inner-wrapper").html());
   
    $sidebar.find(".actions").prepend('<a class="custom-checkout-link" href="https://www.merrellaustralia.com.au/checkout/onepage/">Checkout</a>');

    var target = document.getElementById('mini-cart');
    change_triggered = false;

    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if(change_triggered == false){
                change_triggered = true;
                dom_changed();
                setTimeout(function(){change_triggered = false;},500)
            }
        });
    });

    var config = { attributes: true, childList: true, characterData: true, subtree: true, attributeOldValue: true, characterDataOldValue: true };
    observer.observe(target, config);

    jQuery(document).ajaxComplete(function(){
        setTimeout(function(){
            if($ajaxEnabled == true){
                $sidebar.find('#sidebar-cart-items').html(jQuery("#topCartContent .inner-wrapper").html());
                if($sidebar.find(".actions .custom-checkout-link").length < 1){
                    $sidebar.find(".actions").prepend('<a class="custom-checkout-link" href="https://www.merrellaustralia.com.au/checkout/onepage/">Checkout</a>');    
                };
                jQuery("#sidebar-cart-overlay").css('display','block').stop().animate({opacity:1},600);
                jQuery("#sidebar-cart").stop().animate({right:0},400);
                jQuery("#sidebar-cart-items").css('height',jQuery(window).height() - jQuery(".sidebar-cart-title").height());
            };
        }, 100);
    });

    function dom_changed(){
        if($ajaxEnabled == true){
            jQuery("#sidebar-cart-overlay").css('display','block').stop().animate({opacity:1},600);
            jQuery("#sidebar-cart").stop().animate({right:0},400);    
        }
    };

    jQuery(".opt4-v2").on('click','.top-cart .block-title, .mob-cart',function(e){
        e.stopPropagation();
        e.preventDefault();
        $ajaxEnabled = true;
        jQuery("#sidebar-cart-overlay").css('display','block').stop().animate({opacity:1},600);
        jQuery("#sidebar-cart").stop().animate({right:0},400);
        jQuery("#sidebar-cart-items").css('height',jQuery(window).height() - jQuery(".sidebar-cart-title").height());
    });

    jQuery(".opt4-v2").on('click','.button.btn-cart',function(e){
        $ajaxEnabled = true;    
    });   

    jQuery(".opt4-v2").on('click','#sidebar-cart .sidebar-cart-close-button',function(e){
        e.stopPropagation();
        e.preventDefault();
        jQuery("#onChange").removeClass('expanded');
        jQuery("#sidebar-cart-overlay").stop().animate({opacity:0},600,function(){
            jQuery("#sidebar-cart-overlay").css('display','none');
        });
        jQuery("#sidebar-cart").stop().animate({right:-445},400);
    });

    jQuery(".opt4-v2").on('click','#sidebar-cart-overlay',function(e){
        e.stopPropagation();
        jQuery("#onChange").removeClass('expanded');
        jQuery("#sidebar-cart-overlay").stop().animate({opacity:0},600,function(){
            jQuery("#sidebar-cart-overlay").css('display','none');
        });
        jQuery("#sidebar-cart").stop().animate({right:-445},400);
    });

    jQuery('.opt4-v2').on('click', '#sidebar-cart .del-product', function(e) {
        e.preventDefault();
        var url      = window.location.href;
        var targetUrl = jQuery(this).attr("href");
        if (confirm('Are you sure you would like to remove this item from the shopping cart?')) {
            if (/cart/i.test(url)){
                window.location.href = targetUrl;
                return true;
            }
            jQuery('#cart-loader').show();
            jQuery.ajax(
                {
                    url: targetUrl,
                    type: "POST",
                    data: '',
                    success: function (data, textStatus, jqXHR) {
                        jQuery('#cart-loader').hide();
                        var obj = jQuery.parseJSON(data);
                        if (obj.status == "ERROR") {
                            jQuery('.messages li').addClass("error-msg").removeClass("success-msg");
                            jQuery('.error-msg').show();
                            jQuery('.messages li').html('<ul><li><span>'+obj.message+'</li></ul>').delay(4000).fadeOut(400);
                        } else {
                            MER.header.clearMiniCart();
                            jQuery('.top-cart').replaceWith(obj.topCart);
                            jQuery('.messages li').addClass("success-msg").removeClass("error-msg");
                            jQuery('.messages li').html('<ul><li><span>Deleted from the cart<span></li></ul>').delay(4000).fadeOut(400);
                        }
                        MER.header.miniCart(true);
                        Enterprise.TopCart.showCart(5);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        jQuery('#cart-loader').hide();
                        jQuery('.cart-popup-msg p span').html('<ul class="messages"><li>Couldn\'t delete from your cart</li></ul>');
                        jQuery('.messages li').addClass("error-msg").removeClass("success-msg");
                        jQuery('.error-msg').show();
                        jQuery('.messages li').html('<ul><li><span>Couldn\'t delete from your cart<span></li></ul>').delay(5000).fadeOut(400);
                        MER.header.miniCart();
                        Enterprise.TopCart.showCart(5);
                    }
            });
        }
    });

    jQuery("#sidebar-cart-items").css('height',jQuery(window).height() - jQuery(".sidebar-cart-title").height());
    jQuery(window).resize(function(){
        jQuery("#sidebar-cart-items").css('height',jQuery(window).height() - jQuery(".sidebar-cart-title").height());
    });

}, "#topCartContent .inner-wrapper");