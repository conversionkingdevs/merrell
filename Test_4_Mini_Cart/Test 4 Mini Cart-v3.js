console.clear();
$j = jQuery.noConflict();
if($j(window).width() > 767){
    $j('body').addClass('opt4-v3');
};


/**************************************
    LAYOUT V3
**************************************/
$j(".opt4-v3 .top-cart #topCartContent").appendTo($j(".opt4-v3 .top-cart .block-title"));

$j(document).ajaxComplete(function() {
    if($j(".opt4-v3 .top-cart .block-title #topCartContent").length < 1){
        $j(".opt4-v3 .top-cart #topCartContent").appendTo($j(".top-cart .block-title"));
    };
    
    $j(".opt4-v3 #topCartContent").click(function(e){
        e.stopPropagation();
    });
});

$j(".opt4-v3 #topCartContent").click(function(e){
    e.stopPropagation();
});



/**************************************
    STYLES
**************************************/
$j('head').append(`
<style type="text/css">
    .opt4-v3 #topCartContent {position:fixed !important; right:20px !important; bottom:20px !important; top:auto !important; left:auto !important; z-index:1000; overflow:visible !important; opacity:0; display:block !important; visibility:hidden !important; -moz-transition: all 0.25s ease-in-out; -webkit-transition: all 0.25s ease-in-out; -o-transition: all 0.25s ease-in-out; width:358px !important; cursor:default}

    .opt4-v3 #topCartContent .inner-wrapper .actions {padding:23px 0 0 0 !important; background-color:#ffffff;}
    .opt4-v3 #topCartContent .inner-wrapper .actions a {float:none; width:100%; height:40px; line-height:40px !important; padding:0 !important; color:#ffffff !important; background-color:#949c17 !important; border-radius:4px !important; font-size:26px !important;}
    .opt4-v3 #topCartContent .inner-wrapper .actions a span {font-size:26px !important; padding:0 !important; color:#ffffff !important; display:block; height:40px; line-height:40px !important; text-transform:uppercase; font-family:"$Knockout47A","Knockout 47 A","Knockout 31 B",Arial,"Helvetica Neue",Helvetica,sans-serif; font-weight:normal !important;}
    .opt4-v3 #topCartContent .inner-wrapper .actions a:hover {background-color:#6b7500 !important;}
    
    .opt4-v3 .block-title.expanded #topCartContent {opacity:1; visibility:visible !important;}

    .opt4-v3 #topCartContent .inner-wrapper {padding:18px 20px 20px 20px; border:none; -webkit-box-shadow: 0px 2px 10px 0px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px 0px rgba(0, 0, 0, 0.3); box-shadow: 0px 2px 10px 0px rgba(0, 0, 0, 0.3);}
    .opt4-v3 #topCartContent .inner-wrapper span {font-weight:normal !important;}
    .opt4-v3 #topCartContent .inner-wrapper .subtotal {display:none !important;}
    .opt4-v3 #topCartContent .inner-wrapper .block-subtitle {background-color:transparent !important; font-size:22px !important; padding:0 !important; font-family:"$Knockout50A","Knockout 50 A","Knockout 31 B",Arial,"Helvetica Neue",Helvetica,sans-serif !important;}
    
    .opt4-v3 #topCartContent .inner-wrapper #mini-cart .item {padding-left:0 !important; padding-right:0 !important;}
    .opt4-v3 #topCartContent .inner-wrapper #mini-cart .item .product-image {width:90px;}
    .opt4-v3 #topCartContent .inner-wrapper #mini-cart .item .product-details {margin-left:110px;} 
    
    .opt4-v3 #topCartContent .inner-wrapper .close-btn {width:15px !important; height:15px !important; background:transparent url("https://i.imgur.com/uKcFmmP.png") no-repeat center center !important; background-size:15px auto !important;}
    
    .opt4-v3 #topCartContent .inner-wrapper .messages li.success-msg {text-align:center;}
    
</style>
`);
