
// Get Shoes
function getShoe (url, callback) {
    jQuery.get(url, function( data ) {
        var html = jQuery.parseHTML(data),
            arr = [];

        jQuery(html).find('.siblings_product li').each(function () {
        
        var item = jQuery(this).find('a').attr('href');

        if (item == undefined || item == null || item == '') {
            item = window.location.href;
        }

        item = item.substring(item.indexOf('-')+1).split('-')
        item.splice(-2)
        item = item.join(' ')

        console.log(item);
        arr.push(item);

        });
        if (callback) {
            callback(arr);
        } else {
            return arr;
        }
    });
}

var arr = {}

jQuery('.category-products li:eq(0)').each(function () {
    var href = jQuery(this).find('h2 a'),
        name = href.text(),
        url = href.attr('href');

    getShoe(url, function (colours) {
        console.log(colours);
        arr[name] = colours;
    });

});

console.log(arr)