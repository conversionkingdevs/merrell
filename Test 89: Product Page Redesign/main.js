function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
    jQuery("body").addClass("opt-89");
    jQuery(".siblings_product_title").text("Select A Colour");

    //add accordions
    jQuery(".product-shop").append('<div class="opt-accordion opt-mobile"> <div class="opt-ac" data-value="DESCRIPTION"> <div class="opt-ac-select"> <span>DESCRIPTION</span> </div><div class="opt-ac-content"></div></div><div class="opt-ac" data-value="TECHNOLOGY"> <div class="opt-ac-select"> <span>TECHNOLOGY</span> </div><div class="opt-ac-content"></div></div><div class="opt-ac" data-value="DELIVERY"> <div class="opt-ac-select"> <span>DELIVERY</span> </div><div class="opt-ac-content"> <div class="std"> <p> <strong>STANDARD DELIVERY TIMES</strong> </p><p>Merrell Australia aims to deliver your order within 2-5 business days almost anywhere in Australia <strong>*</strong>. Merrell Australia delivers products Australia wide using Australia Post.</p><p>Delivery times are estimated between 2-5 days from the date of dispatch, depending on your location: <br>1-2 business days - NSW / ACT <br>2-3 business days - VIC / QLD <br>4-5 business days - SA / TAS <br>7-10 business days - WA / NT <br><strong>*</strong>Please allow additional 1-2 business days on top of the above allocated times for regional areas. </p><p> <strong style="font-size: 12px;">EXPRESS DELIVERY TIMES</strong> </p><p> <a title="Australia Post Website" href="https://auspost.com.au/parcels-mail/calculate-postage-delivery-times/#/" target="_blank">Check via Australia Post</a> for expected delivery time according to your location. Orders are being shipped via Moorebank&nbsp; <span>NSW, 2170.</span> </p><p> </div></div></div><div class="opt-ac" data-value="RETURNS"> <div class="opt-ac-select"> <span>RETURNS</span> </div><div class="opt-ac-content"> <strong>WHAT IS YOUR RETURN POLICY?</strong> </p><p>We’re not happy until you’re 100% satisfied with the fit of your new shoes or items. So if they’re anything short of perfect, simply contact our online customer service team.</p><p>Merrell has a 30 day Australia-wide free shipping returns policy for all online purchases, provided the returns are unworn, in the condition you received them, and in the original packaging.</p><p>• You can return your purchase up to 30 days from date of dispatch <br>• Products must be unworn, in the condition that you received them and in the original box and/or packaging with a return reference number <br>• Australia-wide return shipping is absolutely free</p><p> </div></div><div class="opt-ac" data-value="REVIEWS"> <div class="opt-ac-select"> <span>REVIEWS</span> </div><div class="opt-ac-content"></div></div></div>');
    jQuery(".product-shop").append('<div class="opt-menu opt-desktop"> <div class="opt-menu-header"> <div class="opt-m opt-active" data-value="DESCRIPTION"> <div class="opt-menu-select"> <span>DESCRIPTION</span> </div></div><div class="opt-m" data-value="TECHNOLOGY"> <div class="opt-menu-select"> <span>TECHNOLOGY</span> </div></div><div class="opt-m" data-value="DELIVERY"> <div class="opt-menu-select"> <span>DELIVERY</span> </div></div><div class="opt-m" data-value="RETURNS"> <div class="opt-menu-select"> <span>RETURNS</span> </div></div><div class="opt-m" data-value="REVIEWS"> <div class="opt-menu-select"> <span>REVIEWS</span> </div></div></div><div class="opt-menu-container"> <div class="opt-menu-content opt-active" data-value="DESCRIPTION"></div><div class="opt-menu-content" data-value="TECHNOLOGY"></div><div class="opt-menu-content" data-value="DELIVERY"> <div class="std"> <p> <strong>STANDARD DELIVERY TIMES</strong> </p><p>Merrell Australia aims to deliver your order within 2-5 business days almost anywhere in Australia <strong>*</strong>. Merrell Australia delivers products Australia wide using Australia Post.</p><p>Delivery times are estimated between 2-5 days from the date of dispatch, depending on your location: <br>1-2 business days - NSW / ACT <br>2-3 business days - VIC / QLD <br>4-5 business days - SA / TAS <br>7-10 business days - WA / NT <br><strong>*</strong>Please allow additional 1-2 business days on top of the above allocated times for regional areas. </p><p> <strong style="font-size: 12px;">EXPRESS DELIVERY TIMES</strong> </p><p> <a title="Australia Post Website" href="https://auspost.com.au/parcels-mail/calculate-postage-delivery-times/#/" target="_blank">Check via Australia Post</a> for expected delivery time according to your location. Orders are being shipped via Moorebank&nbsp; <span>NSW, 2170.</span> </p><p> </div></div><div class="opt-menu-content" data-value="RETURNS"> <strong>WHAT IS YOUR RETURN POLICY?</strong> </p><p>We’re not happy until you’re 100% satisfied with the fit of your new shoes or items. So if they’re anything short of perfect, simply contact our online customer service team.</p><p>Merrell has a 30 day Australia-wide free shipping returns policy for all online purchases, provided the returns are unworn, in the condition you received them, and in the original packaging.</p><p>• You can return your purchase up to 30 days from date of dispatch <br>• Products must be unworn, in the condition that you received them and in the original box and/or packaging with a return reference number <br>• Australia-wide return shipping is absolutely free</p><p> </div><div class="opt-menu-content" data-value="REVIEWS"></div></div></div>');

    //add description
    jQuery(".opt-ac[data-value='DESCRIPTION'] .opt-ac-content").append(jQuery('.product-view > .product_description').clone());
    jQuery(".opt-ac[data-value='TECHNOLOGY'] .opt-ac-content").append(jQuery('.product-view > .product_technology').clone());

    jQuery(".opt-menu-content[data-value='DESCRIPTION']").append(jQuery('.product-view > .product_description').clone());
    jQuery(".opt-menu-content[data-value='TECHNOLOGY']").append(jQuery('.product-view > .product_technology').clone());

    jQuery(".opt-menu-content[data-value='RETURNS']").append('<a target="_blank" href="http://www.merrellaustralia.com.au/return-information" class="opt-read-more">read more</a>');
    jQuery(".opt-menu-content[data-value='DELIVERY']").append('<a target="_blank" href="http://www.merrellaustralia.com.au/shipping-information" class="opt-read-more">read more</a>');

    jQuery(".opt-ac[data-value='RETURNS'] .opt-ac-content").append('<a target="_blank" href="http://www.merrellaustralia.com.au/return-information" class="opt-read-more">read more</a>');
    jQuery(".opt-ac[data-value='DELIVERY'] .opt-ac-content").append('<a target="_blank" href="http://www.merrellaustralia.com.au/shipping-information" class="opt-read-more">read more</a>');

    if(jQuery("#product-reviews-list").length == 0) {
        jQuery(".opt-menu-content[data-value='REVIEWS']").append('<p class="opt-no-review hide-item"><a href="#no_review" class="pop-login">Be the first to review this product</a></p>');
        jQuery(".opt-ac[data-value='REVIEWS'] .opt-ac-content").append('<p class="opt-no-review hide-item"><a href="#no_review" class="pop-login">Be the first to review this product</a></p>');
        jQuery(".opt-no-review").click(function(){
            jQuery(".no-rating a").click();
        });
    } else {
        jQuery(".opt-menu-content[data-value='REVIEWS']").append(jQuery('.product-view > #customer-reviews').clone());
        jQuery(".opt-ac[data-value='REVIEWS'] .opt-ac-content").append(jQuery('.product-view > #customer-reviews').clone());
    }

    jQuery(".opt-89.catalog-product-view .product-shop .technology-block").each(function(){
        jQuery(this).append('<div class="opt-right-container"></div>');
        jQuery(this).find("h4").appendTo(jQuery(this).find('.opt-right-container'));
        jQuery(this).find("p.content").appendTo(jQuery(this).find('.opt-right-container'));
    });

    jQuery(".opt-m").click(function(){
        var value = jQuery(this).attr("data-value");
        jQuery(".opt-active").removeClass("opt-active");
        jQuery(this).addClass("opt-active");
        jQuery(".opt-menu-content[data-value='"+value+"']").addClass("opt-active");
    });

    jQuery(".opt-m").click(function(){
        var value = jQuery(this).attr("data-value");
        jQuery(".opt-active").removeClass("opt-active");
        jQuery(this).addClass("opt-active");
        jQuery(".opt-menu-content[data-value='"+value+"']").addClass("opt-active");
    });

    jQuery(".opt-ac").click(function(){
        var value = jQuery(this).attr("data-value");
        if(!jQuery(this).hasClass("opt-active")){
            jQuery(".opt-active").removeClass("opt-active");
            jQuery(this).addClass("opt-active");
            jQuery(this).find(".opt-ac-content").addClass("opt-active");
        } else {
            jQuery(".opt-active").removeClass("opt-active");
        }
    });
}, ".breadcrumbs");

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

if (dataLayer[0]['category'].indexOf('Footwear') >= 0) {
    jQuery('body').addClass('opt59');
    jQuery('.opt59 .product-img-box').append('<div class="product_description opt59-group"><h2><span>This Product is great for</span></h2></div>');
    var html = "";
    if (dataLayer[0]['category'].indexOf('Casual') >= 0) {
        html = '<div class="opt59-items"> <div class="opt59-item"> <div class="opt59-icon"><img src="//cdn.optimizely.com/img/6092490016/dbe749b28b3d406e8fbc3fdcafd6d7ee.png"></div></div><div class="opt59-item"> <div class="opt59-icon"><img src="//cdn.optimizely.com/img/6092490016/d9234a0466d64e0d9e8f316bf4306d4b.png"></div></div><div class="opt59-item"> <div class="opt59-icon"><img src="//cdn.optimizely.com/img/6092490016/7d84f2859ff34ac1ba301d845f8a5436.png"></div></div></div>';
        jQuery('.opt59 .opt59-group').append(html);
    } else {
        html = '<div class="opt59-items"> <div class="opt59-item"> <div class="opt59-icon"><img src=""></div></div><div class="opt59-item"> <div class="opt59-icon"><img src=""></div></div><div class="opt59-item"> <div class="opt59-icon"><img src=""></div></div></div>';
        jQuery('.opt59 .opt59-group').append(html);
        //Lists
        var listtech = ["tech_backpacking_grade","tech_insulated","tech_quick_dry","tech_sun_protection","tech_traction","tech_vegan_friendly","tech_ventilated","tech_waterproof","tech_wind_protection","tech_vibram","tech_q_form","tech_m_grip","tech_ventilated","tech_m_move","tech_waterproof_footwear","tech_m_fresh"];
        var ic1 = "//cdn.optimizely.com/img/6092490016/745e604e3a3a4728ae679731a26a541f.png";
        var ic2 = "//cdn.optimizely.com/img/6092490016/688cf15223404ddf8e05c1be0833bf85.png";
        var ic3 = "//cdn.optimizely.com/img/6092490016/1708f38f4a504ed6b0f4d7c74757cdb1.png";
        var ic4 = "//cdn.optimizely.com/img/6092490016/0cda5f9569a94a86b73a0a4a0958427c.png";
        var ic5 = "//cdn.optimizely.com/img/6092490016/bba575a278484620b42b03893888f2e9.png";
        var ic6 = "//cdn.optimizely.com/img/6092490016/9db5fd824d2e4789aaff56632631bc0f.png";
        var ic7 = "//cdn.optimizely.com/img/6092490016/dbe749b28b3d406e8fbc3fdcafd6d7ee.png";
        var ic8 = "//cdn.optimizely.com/img/6092490016/aa540c036ac440178963aa0ff461bc54.png";
        var ic9 = "//cdn.optimizely.com/img/6092490016/7d84f2859ff34ac1ba301d845f8a5436.png";
        var ic10 = "//cdn.optimizely.com/img/6092490016/d3abc039ddf44310b27dcfae6e3f91c5.png";
        var ic11 = "//cdn.optimizely.com/img/6092490016/b28a3883e3474fce823e520ff44a5e53.png";
        var ic12 = "//cdn.optimizely.com/img/6092490016/d9234a0466d64e0d9e8f316bf4306d4b.png";
        var ic13 = "//cdn.optimizely.com/img/6092490016/4f9f88ac8bd546bea61f2ee34f5dc62b.png";
        var listdata= {
            tech_backpacking_grade: {
                icon0: ic10, icon1: ic2, icon2: ic8
            },
            tech_insulated: {
                icon0: ic12, icon1: ic2, icon2: ic8
            },
            tech_quick_dry: {
                icon0: ic4, icon1: ic3, icon2: ic9
            },
            tech_sun_protection: {
                icon0: ic7, icon1: ic12, icon2: ic6
            },
            tech_traction: {
                icon0: ic5, icon1: ic3, icon2: ic2
            },
            tech_vegan_friendly: {
                icon0: ic12, icon1: ic1, icon2: ic7
            },
            tech_ventilated: {
                icon0: ic11, icon1: ic6, icon2: ic1
            },
            tech_waterproof: {
                icon0: ic10, icon1: ic3, icon2: ic4
            },
            tech_wind_protection: {
                icon0: ic9, icon1: ic2, icon2: ic5
            },
            tech_vibram: {
                icon0: ic3, icon1: ic10, icon2: ic2
            },
            tech_q_form: {
                icon0: ic9, icon1: ic12, icon2: ic7
            },
            tech_m_grip: {
                icon0: ic10, icon1: ic3, icon2: ic5
            },
            tech_m_move: {
                icon0: ic11, icon1: ic6, icon2: ic1
            },
            tech_waterproof_footwear: {
                icon0: ic3, icon1: ic3, icon2: ic4
            },
            tech_m_fresh: {
                icon0: ic9, icon1: ic7, icon2: ic13
            }
        };
        
        var group = [];
        var item = "";
        //check what icons will be on this page
        jQuery('.opt59 .product_technology .technology-block').each(function() {
            item = jQuery(this).attr('class').split('technology-block ')[1];
            if (listtech.indexOf(item) >= 0) {
                group.push(item);
            }
        });
        
        for (i = 0; i < 3; i++) {
            jQuery('.opt59 .opt59-item:eq('+i+') img').attr('src', listdata[group[0]]["icon"+i]);
        }
    }
}
