function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
defer(function(){
    if(jQuery(window).width() <= 640){
        jQuery('body').addClass('opt3-v1');
        jQuery('body').addClass('opt3-v2');
        
        /**************************************
            LAYOUT V1
        **************************************/ 
        jQuery("#sidebar-nav").remove();
        jQuery(".header-mobile-links .mob-link").remove();
        
        jQuery(".logo-container").prepend('<a class="menu-button" href="#"><span class="lines"><span class="line"></span><span class="line"></span><span class="line"></span><span class="line"></span></span></a>');
        
        jQuery("body").prepend('<div id="sidebar-nav"> <div class="sidebar-nav-menu" /> </div>');
        
        jQuery("#sidebar-nav").prepend( '<div class="sidebar-logo" />' );
        jQuery("#sidebar-nav .sidebar-logo").html(jQuery(".logo").html());
        jQuery("#sidebar-nav").prepend('<a class="menu-button" href="#"><span class="lines"><span class="line"></span><span class="line"></span><span class="line"></span><span class="line"></span></span></a>');
        jQuery("#sidebar-nav .sidebar-nav-menu").append( jQuery(".nav-container").html() );
        jQuery("#sidebar-nav .sidebar-nav-menu #nav").append( jQuery(".quick-links .links ").html() );
        
        jQuery("#sidebar-nav .sidebar-nav-menu #nav li ").each(function(){
            if(jQuery(this).find('ul').length){
                jQuery(this).addClass('dropdown');
            };
        });
        jQuery(".opt3-v1 #sidebar-nav .sidebar-nav-menu li p span").each(function(){
        jQuery(this).parent().replaceWith('<a>FEATURED</a>');
        });
        jQuery(".logo-container .menu-button").bind('click tap touchstart touch',function(e){
            e.preventDefault();
            jQuery("body").addClass('menu-open');
            jQuery(this).css('opacity',0);
            jQuery("#sidebar-nav").animate({
                left:0
            },400, function () {
                jQuery("body").addClass('menu-open-done');
            });
            jQuery(".wrapper, #sli_autocomplete").animate({
                left:280
            },400, function () {
                jQuery('#sli_autocomplete').addClass('optSlideout');
            });
        });
        function closeMenu () {
            jQuery("body").removeClass('menu-open');
            
            jQuery("#sidebar-nav").animate({
                left:-280
            },400,function(){
                jQuery(".logo-container .menu-button").css('opacity',1);
                jQuery("body").removeClass('menu-open-done');
            });
            
            jQuery(".wrapper").animate({
                left:0
            },400, function () {
                jQuery('#sli_autocomplete').removeClass('optSlideout');
            });
        }
        
        jQuery("#sidebar-nav .menu-button").bind('click tap touchstart touch',function(e){
            e.preventDefault();
            closeMenu();
        });
        
        jQuery('.wrapper').bind('click tap touchstart touch',function (e) {
            if (jQuery('body').hasClass('menu-open-done')) {
                e.preventDefault();
                closeMenu();
            }
        });
        
        jQuery("#sidebar-nav .sidebar-nav-menu li.level0 > a").bind('click tap touch',function(e){
            e.preventDefault();
            var li = jQuery(this).parent();
            jQuery(li).toggleClass('open');
            jQuery(li).find('.menu-level1').slideToggle();
        });
        
        jQuery("#sidebar-nav .sidebar-nav-menu li.level1 > a").bind('click tap touch',function(e){
            e.preventDefault();
            var li = jQuery(this).parent();
            jQuery(li).toggleClass('open');
            jQuery(li).find('.menu-level2').slideToggle();
        });
        
        // Remove benefits
        jQuery('#nav > li:nth-child(2) > div > div > ul > li:nth-child(4),#nav > li:nth-child(1) > div > div > ul > li:nth-child(4)').remove();
        //jQuery('.level2.mob-extra,.level1.mob-extra').remove();
        jQuery('.level2.mob-extra').remove();    
    }
}, ".header-mobile-links");