
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           hj('trigger', 'Test_3A_Mobile_Menu');
          	hj('tagRecording', ['Test 3A Mobile Menu', 'Variation #1']);
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
    if(jQuery(window).width() <= 640){
        console.clear();
        $ = jQuery.noConflict();
        $('body').addClass('opt3-v1');
        
        /**************************************
            LAYOUT V1
        **************************************/ 
        $("#sidebar-nav").remove();
        $(".header-mobile-links .mob-link").remove();
        
        $(".logo-container").prepend('<a class="menu-button" href="#"><span class="lines"><span class="line"></span><span class="line"></span><span class="line"></span><span class="line"></span></span></a>');
        
        $("body").prepend('<div id="sidebar-nav"> <div class="sidebar-nav-menu" /> </div>');
        
        $("#sidebar-nav").prepend( '<div class="sidebar-logo" />' );
        $("#sidebar-nav .sidebar-logo").html($(".logo").html());
        $("#sidebar-nav").prepend('<a class="menu-button" href="#"><span class="lines"><span class="line"></span><span class="line"></span><span class="line"></span><span class="line"></span></span></a>');
        $("#sidebar-nav .sidebar-nav-menu").append( $(".nav-container").html() );
        $("#sidebar-nav .sidebar-nav-menu #nav").append( $(".quick-links .links ").html() );
        
        $("#sidebar-nav .sidebar-nav-menu #nav li ").each(function(){
            if($(this).find('ul').length){
                $(this).addClass('dropdown');
            };
        });
        
        $(".opt3-v1 #sidebar-nav .sidebar-nav-menu li p span").each(function(){
        $(this).parent().replaceWith('<a>FEATURED</a>');
        });
        
        
        $(".logo-container .menu-button").bind('click tap touchstart touch',function(e){
            e.preventDefault();
            $("body").addClass('menu-open');
            $(this).css('opacity',0);
            $("#sidebar-nav").animate({
                left:0
            },400, function () {
                $("body").addClass('menu-open-done');
            });
        
            $(".wrapper, #sli_autocomplete").animate({
                left:280
            },400, function () {
                jQuery('#sli_autocomplete').addClass('optSlideout');
            });
        });
        function closeMenu () {
            $("body").removeClass('menu-open');
            
            $("#sidebar-nav").animate({
                left:-280
            },400,function(){
                $(".logo-container .menu-button").css('opacity',1);
                $("body").removeClass('menu-open-done');
            });
            
            $(".wrapper").animate({
                left:0
            },400, function () {
                jQuery('#sli_autocomplete').removeClass('optSlideout');
            });
        }
        
        $("#sidebar-nav .menu-button").bind('click tap touchstart touch',function(e){
            e.preventDefault();
            closeMenu();
        });
        
        jQuery('.wrapper').bind('click tap touchstart touch',function (e) {
            if (jQuery('body').hasClass('menu-open-done')) {
                e.preventDefault();
                closeMenu();
            }
        });
        
        $("#sidebar-nav .sidebar-nav-menu li.level0 > a").bind('click tap touch',function(e){
            e.preventDefault();
            $li = $(this).parent();
            $li.toggleClass('open');
            $li.find('.menu-level1').slideToggle();
        });
        
        $("#sidebar-nav .sidebar-nav-menu li.level1 > a").bind('click tap touch',function(e){
            e.preventDefault();
            $li = $(this).parent();
            $li.toggleClass('open');
            $li.find('.menu-level2').slideToggle();
        });
        
        // Remove benefits
        jQuery('#nav > li:nth-child(2) > div > div > ul > li:nth-child(4),#nav > li:nth-child(1) > div > div > ul > li:nth-child(4)').remove();
        //jQuery('.level2.mob-extra,.level1.mob-extra').remove();
        jQuery('.level2.mob-extra').remove();    
    }
}, ".header-mobile-links");