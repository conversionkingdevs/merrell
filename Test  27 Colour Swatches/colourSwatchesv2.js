var styles = window.styles;

//uses the styles list to generate an object that contains all the different colors.
function buildColorObject(){
    for(var style in styles){
        for(var i = 0; i < styles[style].length; i++){
            var color = styles[style][i].color;
            if(colorObject[color] == undefined){
                colorObject[color] = "";
            }
        }
    }
}

//use css colors(https://www.w3schools.com/cssref/css_colors.asp) or hex codes, below is an example list
var colorObject = {  
    "Black":"black",
    "Ash":"grey",
    "Burgundy / Black":"#800000",
    "Noire":"black",
    "Dark Slate":"DarkSlateGrey ",
    "Navy":"navy",
    "Granite / Black":"black",
    "Orange":"orange",
    "Black / Black":"black",
    "Black / Grey":"grey",
    "Light Brown":"#8B4513",
    "Espresso":"brown",
    "Grey / Orange":"orange",
    "Shetland":"black",
    "Black / Red":"red",
    "Clay":"grey",
    "Black / Light Grey":"black",
    "Navy / Racer":"navy",
    "Brown Sugar":"brown",
    "Dark Earth":"brown",
    "Dark Olive":"darkolivegreen",
    "Charcoal":"black",
    "787878":"grey",
    "Blue / White":"blue",
    "Hunter Green":"green",
    "Black Sulphur":"black",
    "Tan":"tan",
    "Moss":"olivedrab",
    "Gun / White":"#7c7267",
    "Black / Scarlet":"black",
    "Dark brown":"#53382c",
    "Folkstone Grey":"blue",
    "Black / Dark Grey":"black",
    "Brown":"#53382c",
    "Olive Black":"#39401E",
    "Walnut":"#BF724A",
    "Fire":"#FF4B02",
    "Dusty Olive":"#757F46",
    "Castlerock":"#BFB798",
    "Dark Grey":"#403F3D",
    "Mudder Orange":"#FF6900",
    "Drizzle / Blue Depths":"#91867a",
    "Taupe":"#BFB6A7",
    "Brindle":"#403322",
    "Beluga":"#D7F0FF",
    "Dark Shadow":"#3E3E40",
    "Silver Sea":"#6BBFA9",
    "White":"#f3f3f3",
    "Beige":"#BFB799",
    "White Print":"#f3f3f3",
    "Red":"#E50012",
    "Black/Violet":"#722BFF",
    "Grey":"#BBB8BF",
    "Sedona/Sage":"#497E7F",
    "Grey / Periwinkle":"#86BCFF",
    "Black Adventure":"#000000",
    "Sleet / Vibrant Coral":"#FF7F64",
    "Castle Rock":"#BFB798",
    "Scarlet":"#7F0400",
    "Cyan":"#53FFF8",
    "Black / Silver":"#000000",
    "Boulder":"#BFA48D",
    "Frost Grey":"#F3FCFF",
    "Chinchilla":"#727678",
    "Windsor Wine":"#522632",
    "Steeple Grey":"#9E908F",
    "Taupe / Silver":"#9E949D",
    "Monument":"#2E2A33",
    "Blackout":"#000000",
    "Sodalite":"#191873",
    "Granite":"#282E2B",
    "Black / Purple":"#000000",
    "Aluminium":"#B5B8B7",
    "Ice":"#AAD6EC",
    "Paloma":"#576E79",
    "Stone":"#626E6F",
    "Olive":"#638242",
    "Women's Zoe Sojourn Knit Q2":"#000000",
    "Surf The Web":"#262E9C",
    "BLACK HEATHER":"#2B2B2B",
    "Asphalt":"#4A4949",
    "Salsa":"#81302A",
    "Poseidon":"#2D6661",
    "Canteen / Brown":"#604C36",
    "Black / Liberty":"#000000",
    "Slate Black":"#424242",
    "Sleet":"#7A7A7A",
    "Vertiver":"#ABB89C",
    "Crown Blue":"#474A61",
    "Black / White":"#000000",
    "Wild Dove":"#EBE9EA",
    "Blue":"#0493EC",
    "Aruba blue":"#5CE2EC",
    "Multi":"#000000",
    "White / Grey":"#f3f3f3",
    "Black Heather":"#000000",
    "Aluminium/Marlin":"#787878",
    "Turbulence":"#465761",
    "Cockatoo Print":"#EBE9EA",
    "Sea Shore":"#B8DEEB"
 };

 var productsToRemove = [
    'http://www.merrellaustralia.com.au/charlotte-tote-3023623-win',
    'http://www.merrellaustralia.com.au/mira-1-4-zip-tech-top-1023694-asp',
    'http://www.merrellaustralia.com.au/austin-3024056-win',
    'http://www.merrellaustralia.com.au/mira-s-s-tech-top-1023828-asp',
    'http://www.merrellaustralia.com.au/delta-leisure-3023290-ckp',
    'http://www.merrellaustralia.com.au/moab-adventure-lce-w-dk-erth-091827w-000 ',
    'http://www.merrellaustralia.com.au/sandspur-rift-strap-slate-blk-344734c-000 ',
    'http://www.merrellaustralia.com.au/geotex-hybrid-1-2-zip-2-0-1023697-blh ',
    'http://www.merrellaustralia.com.au/wander-3023639-asp ',
    'http://www.merrellaustralia.com.au/articulus-short-1021082-chi ',
    'http://www.merrellaustralia.com.au/articulus-short-1021082-cas ',
    'http://www.merrellaustralia.com.au/austin-3024056-win ',
    'http://www.merrellaustralia.com.au/stapleton-softshell-jkt-1023720-cas ',
    'http://www.merrellaustralia.com.au/geotex-fz-hoodie-1023470-blh ',
    'http://www.merrellaustralia.com.au/moab-adventure-lce-w-dk-erth-091827w-000',
    'http://www.merrellaustralia.com.au/white-pine-mid-vent-wp-slat-bl-0012469-000',
    'http://www.merrellaustralia.com.au/wander-3023639-asp',
    'http://www.merrellaustralia.com.au/austin-3024056-win',
    'http://www.merrellaustralia.com.au/stapleton-softshell-jkt-1023720-cas',
    'http://www.merrellaustralia.com.au/wander-3023639-asp',
    'http://www.merrellaustralia.com.au/austin-3024056-win',
    'http://www.merrellaustralia.com.au/charlotte-tote-3023623-win',
    'http://www.merrellaustralia.com.au/mira-1-4-zip-tech-top-1023694-asp',
    'http://www.merrellaustralia.com.au/delta-leisure-3023290-ckp',
    'http://www.merrellaustralia.com.au/mira-s-s-tech-top-1023828-asp',
    'http://www.merrellaustralia.com.au/articulus-short-1021082-cas'
 ];

 

// This function will take a product code and return an object of links & colours for the associated style
function findColours(code, callback) {
        //var style = products[code],
        styleObj = styles[code];
        if (callback) {
            callback(styleObj);
        } else {
            return styleObj;
        }
}

function compare (a, b) {
    if (a == b) {
        return 'darker';
    } else {
        return '';
    }
}

function renderStyle (optHref, code, styleobj) {
    if (styleobj) {
        var el = jQuery('.products-grid li.item div[data-direct-url="'+optHref+'"]:eq(0)').parents('li');
        $(el).addClass("foundColor");
        //el.find('.sli_conditional a').remove(); // Remove More Colours Area
        el.find('.product-name').before('<div class="swatchContainer" data-opt="'+code+'"></div>');
        for (var x = 0; x < styleobj.length; x++) {
            if(colorObject[styleobj[x].color] == undefined){
                console.log(styleobj);
            }
            //var swatchUrl = styleobj[x].url.split("https").join("http");
            var swatchUrl = styleobj[x].url;
            var html = '<a class="optCircle '+compare(optHref, swatchUrl)+'"  href='+styleobj[x].url+' style="background-color:'+colorObject[styleobj[x].color]+';"></a>';
            jQuery('.swatchContainer[data-opt="'+code+'"]').append(html);
        }
    } else {
        //console.log('Object is Null');
    } 
}

function getProductCodeFromURL(url){
    var temp = url.split("-");
    var tail = temp.pop();
    var head = temp.pop();
    return head+"."+tail;
}

function checkItems () {

    jQuery(".swatchContainer").remove();

    for(var i = 0; i < productsToRemove.length; i ++){
        var item = jQuery("a[href='"+productsToRemove[i]+"']");
       if(item.length > 0){
           jQuery(item).parents("li").remove();
       }
    }

    var items = jQuery('.products-grid .item');
    
    items.each(function(){
        var optHref = jQuery(this).find('.product-image').attr('href');
        var prodCode = getProductCodeFromURL(optHref);
        findColours(prodCode, function (obj) {
            renderStyle(optHref, prodCode, obj);
        });
    });



    jQuery("body").removeClass("opt-checking");
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function start () {
    jQuery("body").addClass("opt-27");
    checkItems();
    jQuery(document).on('sli-color-update', function (event) {
        checkItems();
    });
}
defer (start, '.item');