// function to check login is selected

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function removeBillingAndShippingDetails () {
    jQuery('.opt_BillingCont').remove();
    jQuery('.cart-summary').removeClass('opt_Added');
}

function matchHeights () {
    var cartheight = jQuery('.opc-progress-container').height(),
        addressheight = jQuery('.opt_BillingCont').height(),
        maxheight = cartheight + addressheight;
    
    if (jQuery(window).width() > 640) {
        jQuery('#checkoutSteps .section:not(#opc-login)').css('min-height', maxheight);
    } else {
        jQuery('#checkoutSteps .section:not(#opc-login)').css('min-height', '1px');
    }
}

function updatePrice () {
//    jQuery('.price-row-total .title').text('Total:');
    if (jQuery('.opt_TotalPrice').length == 0) {
        jQuery('.cart-summary .sub-title').after('<div class="opt_TotalPrice"></div>');
        jQuery('.opt_TotalPrice').text(jQuery('.total .price').text());
    }
}

function addClickForCheckout () {
    if (!jQuery('.opc-progress-container .cart-summary').hasClass('opt_AddedClick')) {
        jQuery('.opc-progress-container .cart-summary').click(function () {
            jQuery(this).closest('.opc-progress-container').toggleClass('opt_ShowSummary');
            jQuery('.opt_BillingCont').toggleClass('opt_ShowSummary');
            var offset = jQuery('.opt_StepContainer').height() + jQuery('.opt_ShowSummary:eq(0)').height() + 30;
            jQuery('.opt_BillingCont').css('top', offset);
        }).addClass('opt_AddedClick');
    }
}

function addBillingAndShippingDetails () {
    setTimeout(function () {
        if (jQuery('.opt_BillingCont').length == 0 ){
            jQuery('.cart-summary').addClass('opt_Added');
            var billing_firstName = jQuery('input[name="billing[firstname]"]').val(),
                billing_lastName = jQuery('input[name="billing[lastname]"]').val(),
                billing_fullName = billing_firstName+' '+billing_lastName,
                billing_address = jQuery('input[name="billing[street][]"]').val(),
                billing_postcode = parseInt(jQuery('input[name="billing[suburb_postcode]"]').val()),
                billing_phone = jQuery('input[name="billing[telephone]"]').val();

            var shipping_firstName = jQuery('input[name="shipping[firstname]"]').val(),
                shipping_lastName = jQuery('input[name="shipping[lastname]"]').val(),
                shipping_fullName = shipping_firstName+' '+shipping_lastName,
                shipping_address = jQuery('input[name="shipping[street][]"]').val(),
                shipping_postcode = parseInt(jQuery('input[name="shipping[suburb_postcode]"]').val()),
                shipping_phone = jQuery('input[name="shipping[telephone]"]').val();
                
            // get information
            var html = ' <div class="opc-progress-container opt_BillingCont" id="col-right-opcheckout"> <div class="opt_Addresses"> <div colspan="2"> <div class="opt_Billing opt_Title">Billing Address</div><p class="opt_Name">'+billing_fullName+'</p><p class="opt_Address">'+billing_address+'</p><p class="opt_Postcode">'+billing_postcode+'</p><p class="opt_Phone">'+billing_phone+'</p></div><div colspan="2"> <div class="opt_Shipping opt_Title">Shipping Address</div><p class="opt_Name">'+shipping_fullName+'</p><p class="opt_Address">'+shipping_address+'</p><p class="opt_Postcode">'+shipping_postcode+'</p><p class="opt_Phone">'+shipping_phone+'</p></div></div></div>'
            jQuery('.opc-progress-container#col-right-opcheckout:not(.opt_BillingCont)').after(html);
        }
    },150)
}

defer(function () {
  jQuery('body').removeClass('opt_LoginSelected opt_Billing opt_Shipping opt_ShippingMethod opt_Payment');

  setInterval(function () {
      if (jQuery('#opc-login').hasClass('active') && !jQuery('body').hasClass('opt_LoginSelected')) {
          jQuery('body').removeClass('opt_LoginSelected opt_Billing opt_Shipping opt_ShippingMethod opt_Payment');
          jQuery('body').addClass('opt_LoginSelected');
          console.log('Login')
          removeBillingAndShippingDetails();
          setTimeout(function () {
          updatePrice();
          addClickForCheckout();
          },1000)
      } else if (jQuery('#opc-billing').hasClass('active') && !jQuery('body').hasClass('opt_Billing')) {
          jQuery('body').removeClass('opt_LoginSelected opt_Billing opt_Shipping opt_ShippingMethod opt_Payment');
          jQuery('body').addClass('opt_Billing');
          console.log('Billing')
          removeBillingAndShippingDetails();
          setTimeout(function () {
              updatePrice();
              addClickForCheckout();
          },1000)
      } else if (jQuery('#opc-shipping').hasClass('active') && !jQuery('body').hasClass('opt_Shipping')) {
          jQuery('body').removeClass('opt_LoginSelected opt_Billing opt_Shipping opt_ShippingMethod opt_Payment');
          jQuery('body').addClass('opt_Shipping');
          console.log('Shipping')
          removeBillingAndShippingDetails();
          setTimeout(function () {
              updatePrice();
              addClickForCheckout();
          },1000)
      } else if (jQuery('#opc-shipping_method').hasClass('active') && !jQuery('body').hasClass('opt_ShippingMethod')) {
          jQuery('body').removeClass('opt_LoginSelected opt_Billing opt_Shipping opt_ShippingMethod opt_Payment');
          jQuery('body').addClass('opt_ShippingMethod');
          console.log('ShippingMethod');
          addBillingAndShippingDetails();
          matchHeights();
          setTimeout(function () {
              updatePrice();
              addClickForCheckout();
          },1000);
      } else if (jQuery('#opc-payment').hasClass('active') && !jQuery('body').hasClass('opt_Payment')) {
          jQuery('body').removeClass('opt_LoginSelected opt_Billing opt_Shipping opt_ShippingMethod opt_Payment');
          jQuery('body').addClass('opt_Payment');
          console.log('Payment');
          addBillingAndShippingDetails();
          matchHeights();
          setTimeout(function () {
              updatePrice();
              addClickForCheckout();
          },1000)
      }
  },50);

  var html = '<div class="opt_StepContainer"> <div class="opt_Step opt_Step_Billing"> <p>1.</p><p>Billing Info</p></div><div class="opt_Step opt_Step_Shipping"> <p>2.</p><p>Shipping Address</p></div><div class="opt_Step opt_Step_ShippingMethod"> <p>3.</p><p>Shipping Options</p></div><div class="opt_Step opt_Step_Payment"> <p>4.</p><p>Payment Info</p></div></div>'

  jQuery('.opc-wrapper').before(html);

  jQuery('.opc-header-steps')
  .removeClass('opc-header-steps').addClass('logo-container opt_Heading')
  .html('<h2>SECURE CHECKOUT</h2>');

  jQuery('.opt_StepContainer .opt_Step').click(function () {
      var name = jQuery(this).find('p:eq(1)').text();
      jQuery('.opt_ShowSummary').removeClass('opt_ShowSummary');
      switch (name) {
          case "Billing Info":
              jQuery('.opc #opc-billing.allow .step-title h2').click();
              break;
          case "Shipping Address":
              jQuery('.opc #opc-shipping.allow .step-title h2').click();
              break;
          case "Shipping Options":
              jQuery('.opc #opc-shipping_method.allow .step-title h2').click();
              break;
          case "Payment Info":
              jQuery('.opc #opc-payment.allow .step-title h2').click();
              break;
          default:
              window.location.reload();
              break;
      }
  })

  matchHeights();
  jQuery(window).resize(function () {
      matchHeights();
  });
}, '.opc-wrapper')

