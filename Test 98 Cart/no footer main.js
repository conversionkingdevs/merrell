
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
    console.clear();
    $ = jQuery.noConflict();
    if($('body').hasClass('checkout-cart-index')){
        $('body').addClass('opt98');    
    };

    /**************************************
        LAYOUT
    **************************************/ 
    $(".opt98 .purchase-process").text('Checkout now');
    $(".opt98 .purchase-process").clone().appendTo($(".opt98 .page .page-title.title-buttons"));

    $('<div class="vouchers-container" />').insertBefore($(".opt98 .page .totals"));
    $vouchers = $(".vouchers-container");
    $vouchers.append('<div class="text"><img src=https://i.imgur.com/PJio15C.png /><p>Got a loyalty voucher?</p></div>');
    $vouchers.append('<div class="form"><form id="vouchers-form"><input type="text" placeholder="Enter loyalty voucher number" /><input type="submit" value="Apply" /></form></div>');
    $vouchers.append('<div class="clear" />');


    $(".opt98 .page .cart-collaterals .deals h2").click(function(e){
        e.preventDefault();
        $this = $(this);
        $this.stop().toggleClass('open');
        $this.parent().find('form').stop().slideToggle(200);
    });

    $(".opt98 .shopping-cart-grand-total td:first-child strong").text('Total :');

    $(".opt98").on('submit','#vouchers-form',function(e){
        e.preventDefault();
        
        if($("#vouchers-form input[type='text']").val() != '' && !$("#vouchers-form input[type='submit']").hasClass('clear')){
            $("#vouchers-form input[type='submit']").addClass('clear').val('Clear');
            $("#vouchers-form input[type='text']").attr('disabled',true);
            setCookie('voucher', $("#vouchers-form input[type='text']").val(), 1)
            jQuery(".vouchers-container p").html('Your voucher has been added! <br> It will be applied at checkout.');
            jQuery(".vouchers-container p").addClass("valid");
        } else {
            $("#vouchers-form input[type='submit']").removeClass('clear').val('Apply');
            $("#vouchers-form input[type='text']").val('');
            $("#vouchers-form input[type='text']").attr('disabled',false);
            setCookie('voucher', $("#vouchers-form input[type='text']").val(), 1)
        };
    });


    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}, ".purchase-process");