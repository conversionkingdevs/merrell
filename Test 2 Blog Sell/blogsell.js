
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

function start(){
    $("body").addClass("opt-2");
    $(".pagetitle").prependTo('.post');
    $(".opt-container").remove();
    $(".post").prepend('<div class="opt-container"></div>');

    $(".opt-container").append('<div class="opt-desktop"></div>');
    $(".opt-desktop").append('<div class="opt-tri-background"></div>');
    $(".opt-desktop").append('<div class="opt-content"></div>');
    $(".opt-content").append('<div class="opt-left"></div>');
    $(".opt-content .opt-left").append('<div class="opt-promo-img"><img src="//cdn.optimizely.com/img/6092490016/37c2e3b7e8144569b21894e003b246a1.png" alt="hiking boots"></div>');
    $(".opt-content .opt-left").append('<div class="opt-promo-text"><span class="opt-title">HIKING BOOTS</span><span class="opt-subtitle">FROM<span class="opt-price"> $269.95</span></span></div>');

    $(".opt-content").append('<div class="opt-right"></div>');
    $(".opt-content .opt-right").append('<div class="opt-promo-links"></div>');
    $(".opt-content .opt-promo-links").append('<div class="opt-top"></div>');
    $(".opt-content .opt-top").append('<span>SHOP</span>');
    $(".opt-content .opt-promo-links").append('<div class="opt-bottom"></div>');
    $(".opt-content .opt-bottom").append('<a href="http://www.merrellaustralia.com.au/mens/view-all"><div class="opt-mens"><span>MENS</span></div></a>');
    $(".opt-content .opt-bottom").append('<a href="http://www.merrellaustralia.com.au/women/view-all"><div class="opt-womens">WOMENS</div></a>');

    $(".opt-container").append('<div class="opt-mobile"></div>');
    $(".opt-mobile").append('<div class="opt-promo-text"><span class="opt-title">HIKING BOOTS</span></div>');
    $(".opt-mobile").append('<div class="opt-promo-img"><img src="//cdn.optimizely.com/img/6092490016/37c2e3b7e8144569b21894e003b246a1.png" alt="hiking boots"></div>');
    $(".opt-mobile").append('<div class="opt-promo-text"><span class="opt-subtitle">FROM<span class="opt-price"> $269.95</span></span></div>');
    $(".opt-mobile").append('<div class="opt-promo-links"></div>');
    $(".opt-mobile .opt-promo-links").append('<div class="opt-top"></div>');
    $(".opt-mobile .opt-top").append('<span>SHOP</span>');
    $(".opt-mobile .opt-promo-links").append('<div class="opt-bottom"></div>');
    $(".opt-mobile .opt-bottom").append('<a href="http://www.merrellaustralia.com.au/mens/view-all"><div class="opt-mens"><span>MENS</span></div></a>');
    $(".opt-mobile .opt-bottom").append('<a href="http://www.merrellaustralia.com.au/women/view-all"><div class="opt-womens">WOMENS</div></a>');
}

defer(function(){
    start();
}, ".post");
