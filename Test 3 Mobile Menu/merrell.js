console.clear();
$ = jQuery.noConflict();
$('body').addClass('test-3');

$("#sidebar-nav").remove();


$(".header-mobile-links .mob-link").remove();
$(".logo-container").prepend('<a class="menu-button" href="#"><span class="lines"><span class="line"></span><span class="line"></span><span class="line"></span><span class="line"></span></span></a>')

$("body").prepend('<div id="sidebar-nav"> <div class="sidebar-nav-menu" /> </div><div id="opt-overlay"></div>');

$("#sidebar-nav").prepend( '<div class="sidebar-logo" />' );
$("#sidebar-nav .sidebar-logo").html($(".logo").html())
$("#sidebar-nav").prepend('<a class="menu-button" href="#"><span class="lines"><span class="line"></span><span class="line"></span><span class="line"></span><span class="line"></span></span></a>')
$("#sidebar-nav .sidebar-nav-menu").append( $(".nav-container").html() )
$("#sidebar-nav .sidebar-nav-menu #nav").append( $(".quick-links .links ").html());

//remove FEATURED text
$("#sidebar-nav .sidebar-nav-menu li p span").each(function(){
    $(this).parent().parent().remove();
});

$("#sidebar-nav .sidebar-nav-menu #nav li ").each(function(){
    if($(this).find('ul').length){
        $(this).addClass('dropdown')
    }
});

$(".logo-container .menu-button").click(function(e){
    e.preventDefault();
    $("body").addClass('menu-open')
    $(this).css('opacity',0)
    $("#sidebar-nav").animate({
        left:0
    },400)
    
    $(".wrapper").animate({
        left:280
    },400)
})


$("#sidebar-nav .menu-button, #opt-overlay").click(function(e){
    e.preventDefault();
    e.stopPropagation();
    $("body").removeClass('menu-open');
    
    $("#sidebar-nav").animate({
        left:-280
    },400,function(){
        $(".logo-container .menu-button").css('opacity',1)
    })
    
    $(".wrapper").animate({
        left:0
    },400)
})


$("#sidebar-nav .sidebar-nav-menu li.level0 > a").click(function(e){
    e.preventDefault();
    if(!$(this).parent().hasClass("dropdown")){
        console.log("running");
        window.location.href = $(this).attr("href");
    } else {
        $li = $(this).parent();
        $li.toggleClass('open');
        $li.find('.menu-level1').slideToggle();
    }
})

$("#sidebar-nav .sidebar-nav-menu li.level1 > a").click(function(e){
    e.preventDefault();
    $li = $(this).parent();
    $li.toggleClass('open');
    $li.find('.menu-level2').slideToggle();
});

/**************************************
    STYLES
**************************************/
$('head').append(`
<style type="text/css">
    /***** Overview Module *****/
    .test-3.menu-open {overflow-x:hidden;}
    .test-3 .wrapper {position:relative;}
    .test-3 .main .nav-container {display:none;}
    .test-3 #sidebar-nav {position:absolute; left:-280px; top:0; width:280px; height:100%; background:#fff; z-index:1000;}
    .test-3 #sidebar-nav .menu-level1 {display:none;}
    .test-3 #sidebar-nav .menu-level2 {display:none;}
    
    .test-3 #sidebar-nav .sidebar-nav-menu {clear:both;} 
    
    .test-3 #sidebar-nav #nav > li a {padding-left:20px;}
    .test-3 #sidebar-nav li a {display:block; height:46px; line-height:46px; font-family:"$Knockout50A","Knockout 50 A","Knockout 31 B",Arial,"Helvetica Neue",Helvetica,sans-serif; font-size:20px; text-transform:uppercase;  text-align:left; color:#676768; text-decoration:none; position:relative; }

    .test-3 #sidebar-nav li.level0 {background:#333333}
    .test-3 #sidebar-nav li.level0 a {color:#fff;}
    
    .test-3 #nav .level1:last-child .menu-level2-inner {padding-bottom:15px;}
    

    .test-3 #nav > li > a:after {content:""; display:block; width:2px; height:8px; position:absolute; right: 20px; top:19px; background-color:#676768; transform:rotate(-45deg);}
    .test-3 #nav > li > a:before {content:""; display:block; width:2px; height:8px; position:absolute; right: 20px; top:24px; background-color:#676768; transform:rotate(45deg);}
    
    .test-3 #nav > li.level0 > a:after { background-color:#f3792d; }
    .test-3 #nav > li.level0 > a:before { background-color:#f3792d; }
    
 
    .test-3 #nav li.dropdown > a:after {content:""; display:block; width:12px; height:2px; position:absolute; right: 16px; top:22px; background-color:#f3792d; transform:rotate(0deg)}
    .test-3 #nav li.dropdown > a:before {content:""; display:block; width:2px; height:12px; position:absolute; right: 21px; top:17px; background-color:#f3792d; transform:rotate(0deg)}
    .test-3 #nav li.dropdown.open > a:before {display:none;}
    .test-3 #nav li.dropdown.open > a {color: #f3792d;}
    
    .test-3 #sidebar-nav li.level2 a {font-size:16px; height:22px; line-height:22px; padding-left:30px !important;}
    
    .test-3 #nav .menu-level1-inner {background-color:#484848;}
    .test-3 #nav .menu-level1-inner li.dropdown > a:before, .test-3 #nav .menu-level1-inner li.dropdown > a:after {background-color:#fff;}
    
    .test-3 .sidebar-logo {float:left; margin-top:18px; margin-left: 36px; }
    .test-3 .sidebar-logo img {max-width:135px; display:block;}
    .test-3 .menu-button { display:block; float:left; width:22px; height:18px; line-height:24px; position:relative; text-decoration:none;  position:relative; z-index:150;}
    .test-3 .menu-button:hover {color:#1a1a1a;}
    .test-3 .menu-button span.lines {width:22px; -webkit-transform: rotate(0deg); -moz-transform: rotate(0deg); -o-transform: rotate(0deg); transform: rotate(0deg); cursor: pointer; position:absolute; left:0; z-index:150; display:block;}
    .test-3 .menu-button span.lines span {display:block; position: absolute; height: 4px; width: 100%; background: #f3792d; opacity: 1; left: 0; -webkit-transform: rotate(0deg); -moz-transform: rotate(0deg); -o-transform: rotate(0deg); transform: rotate(0deg); -webkit-transition: .2s ease-in-out; -moz-transition: .2s ease-in-out; -o-transition: .2s ease-in-out; transition: .2s ease-in-out;}
    .test-3 .menu-button span.lines span:nth-child(1) {top: 0px;}
    .test-3 .menu-button span.lines span:nth-child(2), .test-3 .menu-button span:nth-child(3) {top:7px;}
    .test-3 .menu-button span.lines span:nth-child(4) {top:14px;}
    
    .test-3 #sidebar-nav .menu-button {margin:20px 0 26px 20px;}
    
    
    .test-3 .header-container.section .header .normal-header .header-mobile-links.mobile {width:104px;}
    .test-3 .header-container.section .header .normal-header .logo-container .logo {width:135px; float:right;}
    .test-3 .header-container.section .header .normal-header .logo-container {width:65%;}
    .test-3 .header-container.section .header .normal-header .logo-container .menu-button {margin-top:12px;} 
    
</style>
`);